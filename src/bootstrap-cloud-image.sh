#!/bin/bash
set -eux

# Generate an instance name
rand=$(cat /dev/urandom | tr -dc 'a-z' | fold -w 6 | head -n 1)

#instance="kube-node-$rand"
#instance="kube-master-$rand"
instance="ubuntu-cloud-$rand"

# Choose an OS variant (run `osinfo-query os` for a full list)
os_variant="ubuntu18.04"

# Installer/Media localtion
backing_store="bionic-server-cloudimg-amd64.img"

# Cloud-Init seed
seed_location="cloud-init-seed.iso"

# Storage
disk_dir="."
disk_size="10"
disk_path="${disk_dir}/${instance}"

# vCPU & RAM
ram="4096"
vcpus="2"

# Networking
network="network=default"

# Start instance...
sudo virt-install \
--import \
--name $instance \
--disk path=$disk_path.qcow2,format=qcow2,size=$disk_size,backing_store=$backing_store \
--disk $seed_location,device=cdrom,readonly=on \
--cpu host \
--vcpus=$vcpus \
--memory $ram \
--os-variant $os_variant \
--network $network \
--graphics none \
--noautoconsole \
;

# Detach cloud-init seed disk (effective on next boot)
sleep 1;
sudo virsh detach-disk --domain $instance $seed_location --config

