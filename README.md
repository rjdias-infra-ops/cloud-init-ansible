# Cloud-init defaults for Ansible controlled machines


## Contents:
* Command to create an ISO for data source "NoCloud".
* Example script to bootstrap a VM using libvirt.
* Dockerfile.
* [GitLab CI file][gitlab-ci.yml].


## Requirements:
* cdrkit (`genisoimage` utility)  


## How to use:
* Generate a ssh key.
* Copy the ssh **pubkey** and **password** to the `user-data` file placeholders `(...)`.
* Chose one of the two:
  - Run `mkiso.sh` to create the ISO.
  - Use the container provided with the repository (example on `Extras`).
* Bootstrap the VM using the [Ubuntu Cloud 18.04 LTS][ubuntu-cloud] image and also attach the created ISO.


## Extras:
* The GitLab CI is just to build and test the container.

* Run with docker:
```bash
docker build -t cloud-init-ansible:latest .

docker run --rm -it \
-v "$(pwd)/src":/tmp/cloud-init:Z \
cloud-init-ansible:latest
```

* Generate a ssh key:
```bash
ssh-keygen -t rsa -b 4096 -C "CloudInit" -f ~/.ssh/CloudInit
```

* Crypt and salt the password using python:
```python
import getpass; import crypt;
print("SHA512 salted hash:\n", crypt.crypt(getpass.getpass('New password: '), crypt.mksalt(crypt.METHOD_SHA512)), sep='')
```


## Resources:
[Cloud-Init Documentation (latest)](https://cloudinit.readthedocs.io/en/latest/)

[ubuntu-cloud]: https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img
[gitlab-ci.yml]: .gitlab-ci.yml

