FROM docker.io/library/alpine:latest

LABEL maintainer="José Dias <121466-rjdias@users.noreply.gitlab.com>"

ENV \
USER_DATA="user-data" \
META_DATA="meta-data" \
OUTPUT="cloud-init-seed.iso"

COPY ["entrypoint.sh", "/tmp/entrypoint.sh"]

RUN \
chmod 777 /tmp/entrypoint.sh; \
apk add --no-cache cdrkit

VOLUME ["/tmp/cloud-init"]
WORKDIR /tmp/cloud-init
ENTRYPOINT ["/tmp/entrypoint.sh"]

